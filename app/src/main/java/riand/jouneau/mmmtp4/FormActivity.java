package riand.jouneau.mmmtp4;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

public class FormActivity extends AppCompatActivity {

    private EditText mFirstName;
    private EditText mLastName;
    private EditText mCity;
    private EditText mDate;
    private Button mValidate;
    private LinearLayout mTelephone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form);
        initWidgets();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.idReset :
                mFirstName.setText("");
                mLastName.setText("");
                mDate.setText("");
                mCity.setText("");
                break;
            case R.id.idTel :
                System.out.println("Tel enclenché");
                if(mTelephone.getVisibility()==View.GONE) mTelephone.setVisibility(View.VISIBLE);
                else mTelephone.setVisibility(View.GONE);
                break;
            case R.id.idWiki :
                String ville = mCity.getText().toString();
                String url = "http://fr.wikipedia.org/wiki/"+ville;
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(intent);
            default :
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void initWidgets(){
        mFirstName = (EditText) findViewById(R.id.idFirstName);
        mLastName = (EditText) findViewById(R.id.idLastName);
        mDate = (EditText) findViewById(R.id.idDate);
        mCity = (EditText) findViewById(R.id.idCity);
        mValidate = (Button) findViewById(R.id.buttonValidate);
        mTelephone = (LinearLayout) findViewById(R.id.layoutPhone);

        mValidate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Form form = new Form(mFirstName.getText().toString(),mLastName.getText().toString(),
                        mDate.getText().toString(),mCity.getText().toString());

                //Requests.addUser(getApplicationContext(),form);

                if(form.getFirstName()==null || form.getFirstName().equals("")){
                    Toast toast = Toast.makeText(getApplicationContext(), "Un prénom est necessaire, l'entrée ne s'est pas rajoutée", Toast.LENGTH_LONG);
                }else {
                    Intent returnIntent = new Intent();
                    returnIntent.putExtra("form", form);
                    setResult(RESULT_OK, returnIntent);
                    finish();
                }
            }
        });

    }
}
