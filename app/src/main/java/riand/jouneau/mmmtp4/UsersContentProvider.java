package riand.jouneau.mmmtp4;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by trurlich on 19/10/15.
 */
public class UsersContentProvider extends ContentProvider {

    public static final String KEY_ROWID = "_id";
    public static final String KEY_FIRSTNAME= "firstName";
    public static final String KEY_LASTNAME = "lastName";
    public static final String KEY_DATE = "dateofbirth";
    public static final String KEY_CITY = "cityofbirth";
    public static final String KEY_ISDELETE = "isDelete";
    public static final String KEY_OBJECTID = "objectId";

    private static final String DATABASE_NAME = "usersdb";
    private static final String DATABASE_TABLE_USERS = "users";
    private static final String DATABASE_TABLE_DATE="date";
    private static final int DATABASE_VERSION = 11;
    public static String LAST_SYNC_DATE = "dateSync";

    // This must be the same as what as specified as the Content Provider authority
    // in the manifest file.
    private static final String AUTHORITY = "userscontentprovider";
    public static final String PROVIDER_NAME = "userscontentprovider";

    public static final Uri CONTENT_URI = Uri.parse("content://" + PROVIDER_NAME + "/" + DATABASE_TABLE_USERS);
    public static final Uri DATE_URI = Uri.parse("content://"+ PROVIDER_NAME + "/" + DATABASE_TABLE_DATE);

    private static final int Code_URIMATCHER_USERS = 1;
    private static final int Code_URIMATCHER_DATE = 2;
    private static HashMap<String, String> USERS_PROJECTION_MAP;

    private static final String TAG = "DBAdapter";

    private static final String DATABASE_CREATE_USERS =
            "create table "+DATABASE_TABLE_USERS+" ("+KEY_ROWID+" integer primary key autoincrement, "
                    +KEY_FIRSTNAME+" text not null, "
                    +KEY_LASTNAME+" text, "
                    +KEY_DATE+" text, "
                    +KEY_CITY+" text,"
                    +KEY_ISDELETE+" boolean, "
                    +KEY_OBJECTID+" text);";

    static final String DATABASE_CREATE_DATE =
            " CREATE TABLE " + DATABASE_TABLE_DATE +
                    " (_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    " dateSync TEXT NOT NULL);";

    private Context context = null;
    private DatabaseHelper DBHelper;

    private SQLiteDatabase db;

    private static class DatabaseHelper extends SQLiteOpenHelper
    {
        DatabaseHelper(Context context)
        {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db)
        {
            db.execSQL(DATABASE_CREATE_USERS);
            db.execSQL(DATABASE_CREATE_DATE);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
        {
            Log.w(TAG, "Upgrading database from version " + oldVersion
                    + " to "
                    + newVersion + ", which will destroy all old data");
            db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE_USERS);
            db.execSQL("DROP TABLE IF EXISTS " +  DATABASE_TABLE_DATE);
            onCreate(db);
        }
    }

    @Override
    public boolean onCreate() {
        context = getContext();
        DBHelper = new DatabaseHelper(context);
        db = DBHelper.getWritableDatabase();

        /**
         * Create a writable database which will trigger its
         * creation if it doesn't already exist.
         */

        return (db == null)? false:true;
    }

    static final UriMatcher uriMatcher;
    static{
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(PROVIDER_NAME, "users", Code_URIMATCHER_USERS);
        uriMatcher.addURI(PROVIDER_NAME, "date", Code_URIMATCHER_DATE);
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        SQLiteQueryBuilder builder = new SQLiteQueryBuilder();
        switch (uriMatcher.match(uri)) {
            case Code_URIMATCHER_USERS:
                builder.setTables(DATABASE_TABLE_USERS);
                Log.d("DEBUG","Query Client URI : "+uri);
                if (selection ==null ) {
                    builder.setProjectionMap(USERS_PROJECTION_MAP);
                }else{
                    builder.appendWhere(selection);
                    selection = null;
                }
                break;
            case Code_URIMATCHER_DATE:
                builder.setTables(DATABASE_TABLE_DATE);
                Log.d("DEBUG","Query Date URI : "+uri);
                if (selection ==null ) {
                    builder.setProjectionMap(USERS_PROJECTION_MAP);
                }else{
                    builder.appendWhere(selection);
                    selection = null;
                }
                break;

            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }

        Cursor cursor = builder.query(db, projection,
                selection, selectionArgs, null, null, sortOrder);

        cursor.setNotificationUri(getContext().getContentResolver(), uri);

        return cursor;
    }

    @Override
    public String getType(Uri uri) {
        // create a new MIME type "com.example.books" for the values which a returned
        return ContentResolver.CURSOR_DIR_BASE_TYPE + '/' + "riand.jouneau.mmm";
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        SQLiteQueryBuilder builder = new SQLiteQueryBuilder();
        Uri _uri = null;
        switch (uriMatcher.match(uri)) {
            case Code_URIMATCHER_USERS:
                Log.d("DEBUG", "Insert Client URI : " + uri);

                // Insert a user
                long codeInsert = db.insert(DATABASE_TABLE_USERS, "", values);

                // Checking the return code
                if (codeInsert > 0) {
                    _uri = ContentUris.withAppendedId(CONTENT_URI, codeInsert);
                    getContext().getContentResolver().notifyChange(_uri, null);
                } else throw new SQLException("Failed to insert a user into " + uri);

            case Code_URIMATCHER_DATE:
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String today = sdf.format(new Date());
                Log.d("DEBUG", "Insert Date : " + today);
                db.execSQL("INSERT INTO " + DATABASE_TABLE_DATE + " VALUES (null, '" + today + "') ");
                break;
        }
        return _uri;
    }  

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        int count = 0;
        switch (uriMatcher.match(uri)){
            case Code_URIMATCHER_USERS:
                count = db.delete(DATABASE_TABLE_USERS, selection, selectionArgs);
                break;
            case Code_URIMATCHER_DATE:
                count = db.delete(DATABASE_TABLE_DATE, selection, selectionArgs);
                break;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        int rowsUpdated=0;
        switch (uriMatcher.match(uri)){
            case Code_URIMATCHER_USERS:

                break;

            case Code_URIMATCHER_DATE:
                Log.d("DEBUG", "Update Date");
                rowsUpdated = db.update(DATABASE_TABLE_DATE,
                        values,
                        selection,
                        selectionArgs);
                break;

            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }

        return rowsUpdated;
    }

}
