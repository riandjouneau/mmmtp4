package riand.jouneau.mmmtp4;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import android.content.ContentValues;
import android.content.Intent;

import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import com.parse.FindCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

public class ListViewActivity extends Activity {

    private ListView mCustomListView;
    private SimpleAdapter mListAdapter;
    private ArrayList<HashMap<String, String>> listItem;
    private EditText filter;
    private Cursor cursor;

    // Parse id and client key
    private String APPLICATION_ID = "UCTS181XgUQvHxPh6NUQsy7DVooaxLE42SLWQQcf";

    private String CLIENT_KEY = "0aPaoBBLPFAlrcn6sQ1yqNH9wXJTLjQXgehELS9n";

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_view);

        // Enable Local Datastore.
        Parse.enableLocalDatastore(getApplicationContext());

        Parse.initialize(getApplicationContext(), APPLICATION_ID, CLIENT_KEY);

        /*
        ParseObject testObject = new ParseObject("TestObject");
        testObject.put("foo", "bar");
        testObject.saveInBackground();
         */

        //Récupération de la listview créée dans le fichier main.xml
        mCustomListView = (ListView) findViewById(R.id.listviewperso);

        //Création de la ArrayList qui nous permettra de remplire la listView
        listItem = new ArrayList<HashMap<String, String>>();

        //populateDBWithSampleData();

        // Loading all users
        cursor = Requests.selectAllUsers(getApplicationContext());
        //loadList(cursor);
        refreshPostList();

        initFilter();

        // Popup with details for the user clicked
        mCustomListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                HashMap<String, String> map = (HashMap<String, String>) mCustomListView.getItemAtPosition(position);
                AlertDialog.Builder adb = new AlertDialog.Builder(ListViewActivity.this);
                adb.setTitle("Détails Item");
                adb.setMessage("Prénom : " + map.get("firstName") + "\nNom : "
                        + map.get("lastName") + "\nDate Naissance : " + map.get("date")
                        + "\nVille : " + map.get("city"));
                adb.setPositiveButton("Ok", null);
                adb.show();
            }

        });
    }

    private void populateDBWithSampleData() {

        Form form1 = new Form("FirstName1","LastName1","Date1","City1");
        if(Requests.userNumberIteration(getApplicationContext(),form1)<=0){
            Requests.addUser(getApplicationContext(),form1);
        }

        Form form2 = new Form("FirstName2","LastName2","Date2","City2");
        if(Requests.userNumberIteration(getApplicationContext(),form2)<=0){
            Requests.addUser(getApplicationContext(),form2);
        }

        Form form3 = new Form("FirstName3","LastName3","Date3","City3");
        if(Requests.userNumberIteration(getApplicationContext(),form3)<=0){
            Requests.addUser(getApplicationContext(),form3);
        }
    }

    private void loadList(Cursor cursor){
        listItem = new ArrayList<HashMap<String, String>>();

        mListAdapter = new SimpleAdapter(this.getBaseContext(), listItem, R.layout.listview_item,
                new String[]{"img", "firstName", "lastName"}, new int[]{R.id.img, R.id.idFirstName, R.id.idLastName});

        mCustomListView.setAdapter(mListAdapter);

        HashMap<String, String> map;

        while(cursor.moveToNext()) {
            map = new HashMap<String, String>();
            map.put("firstName", cursor.getString(cursor.getColumnIndex(UsersContentProvider.KEY_FIRSTNAME)));
            map.put("lastName", cursor.getString(cursor.getColumnIndex(UsersContentProvider.KEY_LASTNAME)));
            map.put("img", String.valueOf(R.mipmap.ic_launcher));
            map.put("date",cursor.getString(cursor.getColumnIndex(UsersContentProvider.KEY_DATE)));
            map.put("city",cursor.getString(cursor.getColumnIndex(UsersContentProvider.KEY_CITY)));
            listItem.add(map);
        }
        mListAdapter.notifyDataSetChanged();
    }

    private void initFilter(){
        filter = (EditText) findViewById(R.id.idFilter);
        filter.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String stringFilter = filter.getText().toString();
                // If nothing in filter, load all users, else load only those with likeness
                if(stringFilter.equals("")) cursor = Requests.selectAllUsers(getApplicationContext());
                else cursor = Requests.filterResearch(getApplicationContext(), stringFilter);
                loadList(cursor);
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    // Method called by the button "New User"
    public void addItem(View v) {
        Intent intent = new Intent(getApplicationContext(), FormActivity.class);
        startActivityForResult(intent, 1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                // After a result, we reload the list with the new data
                // Note : if we wanted optimisation, we could have keeped previous data and just
                // add the newest to the list, instead of loading again everything
                cursor = Requests.selectAllUsers(getApplicationContext());

                Form form = data.getParcelableExtra("form");
                addUser(form);
                refreshPostList();
                //loadList(cursor);
            }
        } else if (resultCode == Activity.RESULT_CANCELED) {
            // Something
        }
    }

    // Parse method
    private void refreshPostList() {

        listItem = new ArrayList<HashMap<String, String>>();

        mListAdapter = new SimpleAdapter(this.getBaseContext(), listItem, R.layout.listview_item,
                new String[]{"img", "firstName", "lastName"}, new int[]{R.id.img, R.id.idFirstName, R.id.idLastName});

        mCustomListView.setAdapter(mListAdapter);

        ParseQuery<ParseObject> query = ParseQuery.getQuery("User");
        Log.d("DEBUG", "" + "List Sync in progress");

        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> postList, ParseException e) {
                if (e == null) {
                    Cursor c_date = getContentResolver().query(UsersContentProvider.DATE_URI, null, null, null, null);

                    // Date init
                    Date dateSync = null;
                    Date today = new Date();
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                    // Fetching last sync date
                    if(c_date.moveToFirst()) {
                        try {
                            dateSync = sdf.parse(c_date.getString(c_date.getColumnIndex(UsersContentProvider.LAST_SYNC_DATE)));

                            String dateTemp = sdf.format(today);

                            ContentValues newDate = new ContentValues();

                            newDate.put("dateSync", dateTemp);

                            // Date Update
                            getContentResolver().update(
                                    UsersContentProvider.DATE_URI, newDate,null,null);

                        } catch (Exception error) {

                        }
                    }else {
                        // Date insertion only if empty
                        ContentValues newDate = new ContentValues();
                        newDate.put(UsersContentProvider.LAST_SYNC_DATE, sdf.format(today));

                        Log.d("DEBUG","Adding Date");

                        Uri uri = getContentResolver().insert(
                                UsersContentProvider.DATE_URI, newDate);

                    }

                    // Parse Data
                    for (ParseObject post : postList) {

                        // If the user wasn't here, we add it

                        if(post.getBoolean("isDelete") && dateSync!=null && post.getCreatedAt().after(dateSync)){
                            String [] condition = {post.getObjectId()};
                            getContentResolver().delete(
                                    UsersContentProvider.CONTENT_URI, "objectId = ?", condition);
                        }
                        else if (!post.getBoolean("isDelete") && (dateSync==null || post.getCreatedAt().after(dateSync))) {
                            Log.d("DEBUG", "Adding user : " + post.getString("firstName"));
                            ContentValues values = new ContentValues();

                            values.put(UsersContentProvider.KEY_ROWID, post.getString("objectId"));
                            values.put(UsersContentProvider.KEY_FIRSTNAME,
                                    post.getString("firstName"));
                            values.put(UsersContentProvider.KEY_LASTNAME,
                                    post.getString("lastName"));
                            values.put(UsersContentProvider.KEY_DATE,
                                    post.getString("birthDate"));
                            values.put(UsersContentProvider.KEY_CITY,
                                    post.getString("birthCity"));
                            values.put(UsersContentProvider.KEY_ISDELETE,
                                    post.getBoolean("isDelete"));
                            values.put(UsersContentProvider.KEY_OBJECTID,
                                    post.getObjectId());

                            Uri uri = getContentResolver().insert(
                                    UsersContentProvider.CONTENT_URI, values);
                        }
                    }

                    //Cursor cursor = getContentResolver().query(UsersContentProvider.CONTENT_URI, null, null, null, null);
                    cursor = Requests.selectAllUsers(getApplicationContext());
                    // Refreshing the list
                    while(cursor.moveToNext()) {
                        HashMap map = new HashMap<String, String>();
                        map.put("firstName", cursor.getString(cursor.getColumnIndex(UsersContentProvider.KEY_FIRSTNAME)));
                        map.put("lastName", cursor.getString(cursor.getColumnIndex(UsersContentProvider.KEY_LASTNAME)));
                        map.put("img", String.valueOf(R.mipmap.ic_launcher));
                        map.put("date",cursor.getString(cursor.getColumnIndex(UsersContentProvider.KEY_DATE)));
                        map.put("city",cursor.getString(cursor.getColumnIndex(UsersContentProvider.KEY_CITY)));
                        listItem.add(map);
                    }
                    mListAdapter.notifyDataSetChanged();
                } else {
                    Log.d(getClass().getSimpleName(), "Error: " + e.getMessage());
                }
            }
        });
    }

    public void addUser(Form form) {
        ParseObject c = new ParseObject("User");
        c.put("firstName", form.getFirstName());
        c.put("lastName", form.getLastName());
        c.put("birthDate", form.getDate());
        c.put("birthCity", form.getCity());
        c.put("isDelete", false);

        try{
            c.save();
        }catch(Exception e){

        }
    }

}