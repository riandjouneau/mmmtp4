package riand.jouneau.mmmtp4;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

/**
 * Created by trurlich on 19/10/15.
 */
public class Requests {

    public static void addUser(Context context, Form form){
        Uri uri = UsersContentProvider.CONTENT_URI;
        ContentValues values = new ContentValues();
        values.put(UsersContentProvider.KEY_FIRSTNAME, form.getFirstName());
        values.put(UsersContentProvider.KEY_LASTNAME, form.getLastName());
        values.put(UsersContentProvider.KEY_DATE, form.getDate());
        values.put(UsersContentProvider.KEY_CITY, form.getCity());
        Uri returnUri = context.getContentResolver().insert(uri, values);

    }

    public static Cursor selectAllUsers(Context context){
        Uri uri = UsersContentProvider.CONTENT_URI;
        Cursor cursor = context.getContentResolver().query(uri, null, null, null, null);
        return cursor;
    }

    public static int userNumberIteration(Context context, Form form){
        Uri uri = UsersContentProvider.CONTENT_URI;
        String selection = UsersContentProvider.KEY_FIRSTNAME+"='"+form.getFirstName()
                +"' AND "+UsersContentProvider.KEY_LASTNAME+"='"+form.getLastName()
                +"' AND "+UsersContentProvider.KEY_DATE+"='"+form.getDate()
                +"' AND "+UsersContentProvider.KEY_CITY+"='"+form.getCity()+"'";
        Cursor cursor = context.getContentResolver().query(uri,null,selection,null,null);
        return cursor.getCount();
    }

    public static Cursor filterResearch(Context context, String filter){
        Uri uri = UsersContentProvider.CONTENT_URI;
        String selection = UsersContentProvider.KEY_FIRSTNAME+" like '%"+filter
                +"%' OR "+UsersContentProvider.KEY_LASTNAME+" like '%"+filter
                +"%' OR "+UsersContentProvider.KEY_DATE+" like '%"+filter
                +"%' OR "+UsersContentProvider.KEY_CITY+" like '%"+filter+"%'";
        Cursor cursor = context.getContentResolver().query(uri,null,selection,null,null);
        return cursor;
    }
}
