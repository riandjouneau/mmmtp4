package riand.jouneau.mmmtp4;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by trurlich on 29/09/15.
 */

public class Form implements Parcelable {
    private String mFirstName;
    private String mLastName;
    private String mDate;
    private String mCity;
    private String objectId;
    private boolean isDelete;

    public Form(String firstName, String lastName, String date, String city){
        mFirstName = firstName;
        mLastName = lastName;
        mDate = date;
        mCity = city;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel out, int flags) {
        out.writeString(mFirstName);
        out.writeString(mLastName);
        out.writeString(mDate);
        out.writeString(mCity);
    }

    public static final Parcelable.Creator<Form> CREATOR
            = new Parcelable.Creator<Form>() {
        public Form createFromParcel(Parcel in) {
            return new Form(in);
        }

        public Form[] newArray(int size) {
            return new Form[size];
        }
    };

    public Form(Parcel in) {
        mFirstName = in.readString();
        mLastName = in.readString();
        mDate = in.readString();
        mCity = in.readString();
    }

    public String getFirstName() {
        return mFirstName;
    }

    public String getLastName() {
        return mLastName;
    }

    public String getDate() {
        return mDate;
    }

    public String getCity() {
        return mCity;
    }

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public boolean isDelete() {
        return isDelete;
    }
}